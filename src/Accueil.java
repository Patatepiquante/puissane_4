import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.control.ButtonBar.ButtonData ;

import java.util.List;
import java.util.Arrays;
import java.io.File;
import java.util.ArrayList;

public class Accueil extends Application {

    private BorderPane panelCentral;

    private Pendu vPendu;

    private BorderPane titre;

    private int cpt;

    private Scene scene;

    public Scene getScene() {
        return scene;
    }

    private Stage stage;

    private Puissance4test puissance;

    public Stage getStage() {
        return stage;
    }

    private int joueurs;

    public void setJoueurs(int joueurs) {
        this.joueurs = joueurs;
    }

    public int getJoueurs() {
        return joueurs;
    }

    @Override
    public void init() {
        this.vPendu = new Pendu(this);
        this.puissance = new Puissance4test(this);
        this.panelCentral= new BorderPane();
        this.titre = new BorderPane();
        this.cpt = 0;
        this.scene = laScene();
        this.joueurs = 1;
    }

    public int getCpt() {
        return cpt;
    }

    /**
     * @return  le graphe de scène de la vue à partir de methodes précédantes
     */
    public Scene laScene(){
        BorderPane fenetre = new BorderPane();
        fenetre.setTop(this.titre);
        fenetre.setCenter(this.panelCentral);
        return new Scene(fenetre, 400, 200);
    }

    private Pane titreChoix(){
        BorderPane top = new BorderPane();
        top.setStyle("-fx-background-color: #D8E8E8;");
        Text toptext = new Text();
        toptext.setText("Plato");
        toptext.setFont(Font.font("Arial", 40));
        top.setCenter(toptext);
        top.setPadding(new Insets(20));
        return top;
    }
    private Pane fenetreChoix(){
        BorderPane choix = new BorderPane();
        HBox center = new HBox();
        ImageView image = new ImageView(new Image("puissance4.png"));
        ImageView image1 = new ImageView(new Image("pendu.png"));
        image.setFitHeight(80);
        image.setFitWidth(80);
        image1.setFitHeight(80);
        image1.setFitWidth(80);
        Button pendu = new Button("",image1);
        Button p4 = new Button("",image);
        center.setAlignment(Pos.CENTER);
        center.setSpacing(40);
        pendu.setOnAction(new ControleurLancerPendu(this));
        p4.setOnAction(new ControleurLancerPuissance4(this));
        center.getChildren().addAll(p4,pendu);
        choix.setCenter(center);
        return choix;
    }

    public void lancePartie(){
        // A implementer
        this.vPendu.setNiveaubis(this.vPendu.getModelePendu().getNiveau());
        this.modeJeuP();

    }
    public void lancePartiePuissance(){
        this.panelCentral.setCenter(this.puissance.fenetrePuis());
    }
    public void lancePartiePuissanceIa(){
        this.panelCentral.setCenter(this.puissance.fenetrePuisIA());
        this.titre.setTop(this.puissance.titrejeuPuisIa());
    }

    public void modeaccPuis(Stage stage){
        stage.setHeight(300);
        stage.setWidth(500);
        this.titre.setTop(this.puissance.titreaccPuis());
        this.panelCentral.setCenter(this.puissance.fenetreaccueilPuis());
    }

    public void bandeautitreAccueil(){
        this.titre.setTop(titreChoix());
    }

    public void modechoix(Stage stage){
        stage.setHeight(300);
        stage.setWidth(500);
        this.joueurs = 1;
        this.panelCentral.setCenter(fenetreChoix());
        
    }
    public void modejoueurPuis(Stage stage){
        stage.setHeight(800);
        stage.setWidth(1000);
        this.titre.setTop(this.puissance.titrejeuPuis());
        this.panelCentral.setCenter(this.puissance.fenetrePuis());
    }

    public void modeiaPuis(Stage stage){
        stage.setHeight(800);
        stage.setWidth(1000);
        this.titre.setTop(this.puissance.titrejeuPuisIa());
        this.panelCentral.setCenter(this.puissance.fenetrePuisIA());
    }

    public void modeAccueilP(Stage stage){
        stage.setHeight(800);
        stage.setWidth(1000);
        this.titre.setTop(this.vPendu.titreP());
        this.panelCentral.setCenter(this.vPendu.fenetreAccueil());
        this.vPendu.getBJouer().requestFocus();
        this.cpt = 0 ;
    } 

    public void modeJeuP(){
        this.titre.setTop(this.vPendu.titreP());
        this.panelCentral.setCenter(this.vPendu.fenetreJeu());
        this.cpt = 1;
    }
    public Alert popUpMessageGagne(){
        // A implementer
        Alert alert = new Alert(Alert.AlertType.INFORMATION,"Bravo ! Vous avez gagné !",ButtonType.OK);      
        alert.setTitle("Le Puissance 4");
        alert.setHeaderText("Vous avez gagné :)");
        return alert;
    }

    @Override
    public void start(Stage stage) {
        stage.setTitle("IUTEAM'S - La plateforme de jeux de l'IUTO");
        stage.setScene(this.scene);
        this.bandeautitreAccueil();
        this.modechoix(stage);
        this.stage = stage;
        stage.show();
    }

    /**
     * Programme principal
     * @param args inutilisé
     */
    public static void main(String[] args) {
        launch(args);
    }
}