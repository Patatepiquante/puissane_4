import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.RadioButton;

/**
 * Controleur des radio boutons gérant le niveau
 */
public class ControleurNiveau implements EventHandler<ActionEvent> {

    private Pendu vuePendu;


    public ControleurNiveau(MotMystere modelePendu, Pendu vuePendu) {
        // this.modelePendu = modelePendu;
        this.vuePendu = vuePendu;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        // A implémenter
        int indice = 0;
        RadioButton radiobouton = (RadioButton) actionEvent.getTarget();
        String nomDuRadiobouton = radiobouton.getText();
        System.out.println("--------------");
        System.out.println(nomDuRadiobouton);
        System.out.println("--------------");
        switch (nomDuRadiobouton){
            case "Facile" : indice = 0; this.vuePendu.setNiveaubis(indice); break;
            case "Moyen" : indice = 1; this.vuePendu.setNiveaubis(indice); break;
            case "Difficile" : indice = 2; this.vuePendu.setNiveaubis(indice); break;
            case "Expert" : indice = 3; this.vuePendu.setNiveaubis(indice); break;
            default : break;
        }
        
    }
}
