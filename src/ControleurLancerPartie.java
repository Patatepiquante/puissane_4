import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ButtonType;
import java.util.Optional;

/**
 * Contrôleur à activer lorsque l'on clique sur le bouton rejouer ou Lancer une partie
 */
public class ControleurLancerPartie implements EventHandler<ActionEvent> {
    /**
     * modèle du jeu
     */
    private MotMystere modelePendu;
    /**
     * vue du jeu
     **/
    private Pendu vuePendu;
    private Accueil accueil;

    /**
     * @param modelePendu modèle du jeu
     * @param p vue du jeu
     */
    public ControleurLancerPartie(MotMystere modelePendu, Pendu vuePendu, Accueil a) {
        // A implémenter
        this.vuePendu=vuePendu;
        this.modelePendu=modelePendu;
        this.accueil = a;
    }

    /**
     * L'action consiste à recommencer une partie. Il faut vérifier qu'il n'y a pas une partie en cours
     * @param actionEvent l'événement action
     */
    @Override
    public void handle(ActionEvent actionEvent) {
        // A implémenter
        if(this.accueil.getCpt()==0){     // Grace au cpt le if gere l'accueil donc le bouton lancer partie 
            this.accueil.lancePartie();
            this.vuePendu.getBoutonMaison().setDisable(false);
            this.vuePendu.getBoutonClavier().requestFocus();
        }
        else if(this.accueil.getCpt()==1){   // le else gere le jeu donc le bouton nouveau mot
            if(!this.modelePendu.gagne() || !this.modelePendu.perdu()){
                Optional<ButtonType> reponse = this.vuePendu.popUpPartieEnCours().showAndWait(); // on lance la fenêtre popup et on attends la réponse
            // si la réponse est oui
                if (reponse.isPresent() && reponse.get().equals(ButtonType.YES)){
                    System.out.println("Ok !");
                    this.accueil.lancePartie();
                    this.vuePendu.getBoutonMaison().setDisable(false);
                    this.vuePendu.majAffichage();
                    this.vuePendu.getBoutonClavier().requestFocus();
                }
                else{
                    System.out.println("D'ac !");
                }
            }
        }
        
    }
}
