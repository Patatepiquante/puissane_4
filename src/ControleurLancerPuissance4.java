import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;

public class ControleurLancerPuissance4 implements EventHandler<ActionEvent>{

    private Accueil accueil;

    public ControleurLancerPuissance4(Accueil a){

        this.accueil = a;
    }
    
    @Override
    public void handle(ActionEvent event) {
        this.accueil.modeaccPuis(this.accueil.getStage());
        
    }
    
}