import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurRetourChoix implements EventHandler<ActionEvent>{

    private Accueil accueil;

    public ControleurRetourChoix(Accueil a){

        this.accueil = a;
    }
    
    @Override
    public void handle(ActionEvent arg0) {
        this.accueil.modechoix(this.accueil.getStage());
        this.accueil.bandeautitreAccueil();
        
    }
    
}
