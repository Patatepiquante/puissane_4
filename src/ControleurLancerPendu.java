import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;

public class ControleurLancerPendu implements EventHandler<ActionEvent>{

    private Accueil accueil;

    public ControleurLancerPendu(Accueil a){

        this.accueil = a;
    }
    
    @Override
    public void handle(ActionEvent arg0) {
        this.accueil.modeAccueilP(this.accueil.getStage());
        
    }
    
}
