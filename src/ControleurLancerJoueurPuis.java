import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;

public class ControleurLancerJoueurPuis implements EventHandler<ActionEvent>{

    private Accueil accueil;

    public ControleurLancerJoueurPuis(Accueil a){

        this.accueil = a;
    }
    
    @Override
    public void handle(ActionEvent event) {
        this.accueil.modejoueurPuis(this.accueil.getStage());
        
    }
    
}