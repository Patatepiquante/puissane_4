import java.util.ArrayList;
import java.util.List;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class ControleurBoutonJeton implements EventHandler<ActionEvent>{

    private Puissance4test puissance;
    private String jetonR;
    private String jetonJ;
    private Accueil accueil;
    private List<List<Button>> list;
    private JeuPuissance4 model;

    public ControleurBoutonJeton(Puissance4test p,JeuPuissance4 j, Accueil a, String image1, String image2){
        this.puissance = p ;
        this.jetonJ= image1;
        this.jetonR= image2;
        this.accueil = a;
        this.list = new ArrayList<>();
        this.model = j;
    }

    @Override
    public void handle(ActionEvent event){
        
        this.list=this.puissance.getListboutons();
        Button bouton = (Button) event.getSource();
        bouton.setDisable(true);
        
        ImageView imagerouge = new ImageView(new Image(this.jetonR));
        ImageView imagejaune = new ImageView(new Image(this.jetonJ));
        imagejaune.setFitHeight(60);
        imagejaune.setFitWidth(60);
        imagerouge.setFitHeight(60);
        imagerouge.setFitWidth(60);
        int boutonid1 =0;
        int boutonid2 =0;
        int ind1 = 0;
        int ind2 = 0; 
        for(List<Button> l : this.list){
            for(Button s : l){
                if(s==bouton){
                    if(ind1>0){
                        this.list.get(ind1-1).get(ind2).setDisable(false);
                    }
                    boutonid1 = ind1;
                    boutonid2 = ind2;
                }
                ind2+=1;
            }
            ind2=0;
            ind1+=1;
        }
        if(this.accueil.getJoueurs()==1){
            bouton.setGraphic(imagerouge);
            bouton.setText("R");
            this.accueil.setJoueurs(2);
            this.puissance.getProchainjoueur().setGraphic(imagejaune);
        }
        else if(this.accueil.getJoueurs()==2){
            bouton.setGraphic(imagejaune);
            bouton.setText("J");
            this.accueil.setJoueurs(1);
            this.puissance.getProchainjoueur().setGraphic(imagerouge);
        }
        this.model.verification(boutonid1,boutonid2,this.list);
        
        
        
    
    }
}