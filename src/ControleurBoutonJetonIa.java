import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Random;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class ControleurBoutonJetonIa implements EventHandler<ActionEvent>{

    private Puissance4test puissance;
    private String jetonR;
    private String jetonJ;
    private Accueil accueil;
    private InteligenceArtificiel ia;
    private List<List<Button>> list;
    private JeuPuissance4 model;
    private Set<Button> ensemblebouton;
    private Set<Button> boutonsjaune;

    public ControleurBoutonJetonIa(Puissance4test p,JeuPuissance4 j, Accueil a, String image1, String image2){
        this.puissance = p ;
        this.jetonJ= image1;
        this.jetonR= image2;
        this.accueil = a;
        this.list = new ArrayList<>();
        this.model = j;
        this.ia = new InteligenceArtificiel(a,p);
        this.ensemblebouton = new HashSet<>();
        this.boutonsjaune = new HashSet<>();
    }

    @Override
    public void handle(ActionEvent event){
        
        this.list=this.puissance.getListboutons();
        Button bouton = (Button) event.getSource();
        bouton.setDisable(true);
        
        ImageView imagerouge = new ImageView(new Image(this.jetonR));
        ImageView imagejaune = new ImageView(new Image(this.jetonJ));
        imagejaune.setFitHeight(60);
        imagejaune.setFitWidth(60);
        imagerouge.setFitHeight(60);
        imagerouge.setFitWidth(60);
        int id1rouge =0;
        int id2rouge =0;
        int ind1 = 0;
        int ind2 = 0; 
        for(List<Button> l : this.list){
            for(Button s : l){
                if(s==bouton){
                    if(ind1>0){
                        this.list.get(ind1-1).get(ind2).setDisable(false);
                    }
                    id1rouge = ind1;
                    id2rouge = ind2;
                }
                ind2+=1;
            }
            ind2=0;
            ind1+=1;
        }
        bouton.setGraphic(imagerouge);
        bouton.setText("R");
        this.ensemblebouton.add(bouton);
        this.model.verificationIa(id1rouge,id2rouge,this.list,"R");
        Button contre = this.ia.contre(id1rouge,id2rouge,this.list);
        Button alea =null;
        System.out.println(contre==null);
        if(contre==null){
            alea= this.ia.alea(this.list);
            alea.setGraphic(imagejaune);
            alea.setDisable(true);
            alea.setText("J");
        }
        else{
            contre.setGraphic(imagejaune);
            contre.setDisable(true);
            contre.setText("J");
        }
        int id1jaune =0;
        int id2jaune=0;
        int indv1 =0;
        int indv2 = 0;
        for(List<Button> l : this.list){
            for(Button s : l){
                if(s==contre || s==alea){
                    id1jaune=indv1;
                    id2jaune=indv2;
                }
                indv2+=1;
            }
            indv2=0;
            indv1+=1;
        }
        this.model.verificationIa(id1jaune,id2jaune,this.list,"J");
        
    }
}