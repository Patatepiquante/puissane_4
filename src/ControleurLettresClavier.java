import java.util.HashSet;
import java.util.Set;

import javafx.event.EventHandler;
import javafx.scene.input.KeyEvent;


/**
 * Controleur du clavier
 */
public class ControleurLettresClavier implements EventHandler<KeyEvent> {

    /**
     * modèle du jeu
     */
    private MotMystere modelePendu;
    /**
     * vue du jeu
     */
    private Pendu vuePendu;
        // cet ensemble permet que les touche deja appuyer ne fonctionne plus
    private Set<String> ensemble;

    /**
     * @param modelePendu modèle du jeu
     * @param vuePendu vue du jeu
     */
    ControleurLettresClavier(MotMystere modelePendu, Pendu vuePendu){
        // A implémenter
        this.modelePendu=modelePendu;
        this.vuePendu=vuePendu;
        this.ensemble = new HashSet<>();
    }

    /**
     * Actions à effectuer lors du clic sur une touche du clavier
     * Il faut donc: Essayer la lettre, mettre à jour l'affichage et vérifier si la partie est finie
     * @param actionEvent l'événement
     */
    @Override
    public void handle(KeyEvent actionEvent) {
        // A implémenter
        if(this.modelePendu.gagne()==true){
            return ;
        }
        else if(this.modelePendu.perdu()==true){
            return;
        }
        char lettre = actionEvent.getText().charAt(0);
        String lettre2 = String.valueOf(lettre).toUpperCase();
        if(!(this.ensemble.contains(lettre2))){
            lettre = lettre2.charAt(0);
            this.modelePendu.essaiLettre(lettre);
            this.vuePendu.majAffichage();
        }  
        this.ensemble.add(lettre2);
        
        
    }
}
