import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;

public class ControleurAccueilPuis implements EventHandler<ActionEvent>{

    private Accueil accueil;

    public ControleurAccueilPuis(Accueil a){

        this.accueil = a;
    }
    
    @Override
    public void handle(ActionEvent event) {
        this.accueil.modeaccPuis(this.accueil.getStage());
        
    }
    
}