import javafx.scene.control.Button;
import java.util.List;

public class JeuPuissance4 {
    private Accueil accueil;

    public JeuPuissance4(Accueil a){
        this.accueil = a;
    }

    public void verification(int id1, int id2, List<List<Button>> list){
        // regarde a droite
        if(id2<4){
            if(list.get(id1).get(id2).getText()==list.get(id1).get(id2+1).getText()){
                if(list.get(id1).get(id2+1).getText()==list.get(id1).get(id2+2).getText()){
                    if(list.get(id1).get(id2+2).getText()==list.get(id1).get(id2+3).getText()){
                        this.accueil.popUpMessageGagne().showAndWait();
                        this.termineJeu(list);
                    }
                }  
            }
        }
        
        // regarde a gauche
        if(id2>2){
            if(list.get(id1).get(id2).getText()==list.get(id1).get(id2-1).getText()){
                if(list.get(id1).get(id2-1).getText()==list.get(id1).get(id2-2).getText()){
                    if(list.get(id1).get(id2-2).getText()==list.get(id1).get(id2-3).getText()){
                        this.accueil.popUpMessageGagne().showAndWait();
                        this.termineJeu(list);
                    }
                }  
            }
        }
        
        // regarde en bas
        if (id1<3){
            if(list.get(id1).get(id2).getText()==list.get(id1+1).get(id2).getText()){
                if(list.get(id1+1).get(id2).getText()==list.get(id1+2).get(id2).getText()){
                    if(list.get(id1+2).get(id2).getText()==list.get(id1+3).get(id2).getText()){
                        this.accueil.popUpMessageGagne().showAndWait();
                        this.termineJeu(list);
                    }
                }  
            }
        }
        
        //regarde en bas a droite
        if(id1<3 && id2<4){
            if(list.get(id1).get(id2).getText()==list.get(id1+1).get(id2+1).getText()){
                if(list.get(id1+1).get(id2+1).getText()==list.get(id1+2).get(id2+2).getText()){
                    if(list.get(id1+2).get(id2+2).getText()==list.get(id1+3).get(id2+3).getText()){
                        this.accueil.popUpMessageGagne().showAndWait();
                        this.termineJeu(list);
                    }
                }  
            }
        }
        
        //regarde en bas a gauche
        if(id1<3 && id2>2){
            
            if(list.get(id1).get(id2).getText()==list.get(id1+1).get(id2-1).getText()){
                if(list.get(id1+1).get(id2-1).getText()==list.get(id1+2).get(id2-2).getText()){
                    if(list.get(id1+2).get(id2-2).getText()==list.get(id1+3).get(id2-3).getText()){
                        this.accueil.popUpMessageGagne().showAndWait();
                        this.termineJeu(list);
                    }
                }  
            }

        }
        //regarde deux couleur en haut a droite et une couleur en bas a gauche 
        if(id1<5 && id1>1 && id2>0 && id2<5){
            if(list.get(id1).get(id2).getText()==list.get(id1-1).get(id2+1).getText()){
                if(list.get(id1-1).get(id2+1).getText()==list.get(id1-2).get(id2+2).getText()){
                    if(list.get(id1).get(id2).getText()==list.get(id1+1).get(id2-1).getText()){
                        this.accueil.popUpMessageGagne().showAndWait();
                        this.termineJeu(list);
                    }
                }
            }
        }

        //regarde une couleur en haut a droite et deux couleur en bas a gauche 
        if(id1<4 && id1>0 && id2>1 && id2<6){
            if(list.get(id1).get(id2).getText()==list.get(id1+1).get(id2-1).getText()){
                if(list.get(id1+1).get(id2-1).getText()==list.get(id1+2).get(id2-2).getText()){
                    if(list.get(id1).get(id2).getText()==list.get(id1-1).get(id2+1).getText()){
                        this.accueil.popUpMessageGagne().showAndWait();
                        this.termineJeu(list);
                    }  
                }
            }
            
        }
        //regarde deux couleur en haut a gauche et une couleur en bas a droite 
        if(id1<5 && id1>1 && id2>1 && id2<6){
            if(list.get(id1).get(id2).getText()==list.get(id1-1).get(id2-1).getText()){
                if(list.get(id1-1).get(id2-1).getText()==list.get(id1-2).get(id2-2).getText()){
                    if(list.get(id1).get(id2).getText()==list.get(id1+1).get(id2+1).getText()){
                        this.accueil.popUpMessageGagne().showAndWait();
                        this.termineJeu(list);
                    }
                }
            }
            
        }
        //regarde une couleur en haut a gauche et deux couleur en bas a droite 
        if(id1<4 && id1>0 && id2>0 && id2<5){
            if(list.get(id1).get(id2).getText()==list.get(id1+1).get(id2+1).getText()){
                if(list.get(id1+1).get(id2+1).getText()==list.get(id1+2).get(id2+2).getText()){
                    if(list.get(id1).get(id2).getText()==list.get(id1-1).get(id2-1).getText()){
                        this.accueil.popUpMessageGagne().showAndWait();
                        this.termineJeu(list);
                    }
                }
                
            }
        
            
        }
        

        // regarde en haut a droite
        if(id1>2 && id2<4){
            if(list.get(id1).get(id2).getText()==list.get(id1-1).get(id2+1).getText()){
                if(list.get(id1-1).get(id2+1).getText()==list.get(id1-2).get(id2+2).getText()){
                    if(list.get(id1-2).get(id2+2).getText()==list.get(id1-3).get(id2+3).getText()){
                        this.accueil.popUpMessageGagne().showAndWait();
                        this.termineJeu(list);
                    }
                }  
            }
        }
        // regarde en haut a gauche
        if(id1>2 && id2>2){
            if(list.get(id1).get(id2).getText()==list.get(id1-1).get(id2-1).getText()){
                if(list.get(id1-1).get(id2-1).getText()==list.get(id1-2).get(id2-2).getText()){
                    if(list.get(id1-2).get(id2-2).getText()==list.get(id1-3).get(id2-3).getText()){
                        this.accueil.popUpMessageGagne().showAndWait();
                        this.termineJeu(list);
                    }
                }  
            }

        }
        // regarde si il est entre deux couleur a droite et une  a gauche
        if(id2<5 && id2>0){
            if(list.get(id1).get(id2).getText()==list.get(id1).get(id2+1).getText()){
                if(list.get(id1).get(id2+1).getText()==list.get(id1).get(id2+2).getText()){
                    if(list.get(id1).get(id2).getText()==list.get(id1).get(id2-1).getText()){
                        this.accueil.popUpMessageGagne().showAndWait();
                        this.termineJeu(list);
                    }    
                }  
            }
        }
        // regarde si il est entre deux couleur a gauche et une  a droite
        if(id2>1 && id2<6){
            if(list.get(id1).get(id2).getText()==list.get(id1).get(id2-1).getText()){
                if(list.get(id1).get(id2-1).getText()==list.get(id1).get(id2-2).getText()){
                    if(list.get(id1).get(id2).getText()==list.get(id1).get(id2+1).getText()){
                        this.accueil.popUpMessageGagne().showAndWait();
                        this.termineJeu(list);
                    }    
                }  
            }
        }
        

    }

    public void verificationIa(int id1, int id2, List<List<Button>> list,String lettre){
        if(list.get(id1).get(id2).getText()==lettre){
            System.out.println(lettre);
            // regarde a droite
            if(id2<4){
                if(list.get(id1).get(id2).getText()==list.get(id1).get(id2+1).getText()){
                    if(list.get(id1).get(id2+1).getText()==list.get(id1).get(id2+2).getText()){
                        if(list.get(id1).get(id2+2).getText()==list.get(id1).get(id2+3).getText()){
                            this.accueil.popUpMessageGagne().showAndWait();
                            this.termineJeu(list);
                        }
                    }  
                }
            }
            
            // regarde a gauche
            if(id2>2){
                if(list.get(id1).get(id2).getText()==list.get(id1).get(id2-1).getText()){
                    if(list.get(id1).get(id2-1).getText()==list.get(id1).get(id2-2).getText()){
                        if(list.get(id1).get(id2-2).getText()==list.get(id1).get(id2-3).getText()){
                            this.accueil.popUpMessageGagne().showAndWait();
                            this.termineJeu(list);
                        }
                    }  
                }
            }
            
            // regarde en bas
            if (id1<3){
                if(list.get(id1).get(id2).getText()==list.get(id1+1).get(id2).getText()){
                    if(list.get(id1+1).get(id2).getText()==list.get(id1+2).get(id2).getText()){
                        if(list.get(id1+2).get(id2).getText()==list.get(id1+3).get(id2).getText()){
                            this.accueil.popUpMessageGagne().showAndWait();
                            this.termineJeu(list);
                        }
                    }  
                }
            }
            
            //regarde en bas a droite
            if(id1<3 && id2<4){
                if(list.get(id1).get(id2).getText()==list.get(id1+1).get(id2+1).getText()){
                    if(list.get(id1+1).get(id2+1).getText()==list.get(id1+2).get(id2+2).getText()){
                        if(list.get(id1+2).get(id2+2).getText()==list.get(id1+3).get(id2+3).getText()){
                            this.accueil.popUpMessageGagne().showAndWait();
                            this.termineJeu(list);
                        }
                    }  
                }
            }
            
            //regarde en bas a gauche
            if(id1<3 && id2>2){
                
                if(list.get(id1).get(id2).getText()==list.get(id1+1).get(id2-1).getText()){
                    if(list.get(id1+1).get(id2-1).getText()==list.get(id1+2).get(id2-2).getText()){
                        if(list.get(id1+2).get(id2-2).getText()==list.get(id1+3).get(id2-3).getText()){
                            this.accueil.popUpMessageGagne().showAndWait();
                            this.termineJeu(list);
                        }
                    }  
                }

            }
            //regarde deux couleur en haut a droite et une couleur en bas a gauche 
            if(id1<5 && id1>1 && id2>0 && id2<5){
                if(list.get(id1).get(id2).getText()==list.get(id1-1).get(id2+1).getText()){
                    if(list.get(id1-1).get(id2+1).getText()==list.get(id1-2).get(id2+2).getText()){
                        if(list.get(id1).get(id2).getText()==list.get(id1+1).get(id2-1).getText()){
                            this.accueil.popUpMessageGagne().showAndWait();
                            this.termineJeu(list);
                        }
                    }
                }
            }

            //regarde une couleur en haut a droite et deux couleur en bas a gauche 
            if(id1<4 && id1>0 && id2>1 && id2<6){
                if(list.get(id1).get(id2).getText()==list.get(id1+1).get(id2-1).getText()){
                    if(list.get(id1+1).get(id2-1).getText()==list.get(id1+2).get(id2-2).getText()){
                        if(list.get(id1).get(id2).getText()==list.get(id1-1).get(id2+1).getText()){
                            this.accueil.popUpMessageGagne().showAndWait();
                            this.termineJeu(list);
                        }  
                    }
                }
                
            }
            //regarde deux couleur en haut a gauche et une couleur en bas a droite 
            if(id1<5 && id1>1 && id2>1 && id2<6){
                if(list.get(id1).get(id2).getText()==list.get(id1-1).get(id2-1).getText()){
                    if(list.get(id1-1).get(id2-1).getText()==list.get(id1-2).get(id2-2).getText()){
                        if(list.get(id1).get(id2).getText()==list.get(id1+1).get(id2+1).getText()){
                            this.accueil.popUpMessageGagne().showAndWait();
                            this.termineJeu(list);
                        }
                    }
                }
                
            }
            //regarde une couleur en haut a gauche et deux couleur en bas a droite 
            if(id1<4 && id1>0 && id2>0 && id2<5){
                if(list.get(id1).get(id2).getText()==list.get(id1+1).get(id2+1).getText()){
                    if(list.get(id1+1).get(id2+1).getText()==list.get(id1+2).get(id2+2).getText()){
                        if(list.get(id1).get(id2).getText()==list.get(id1-1).get(id2-1).getText()){
                            this.accueil.popUpMessageGagne().showAndWait();
                            this.termineJeu(list);
                        }
                    }
                    
                }
            
                
            }
            

            // regarde en haut a droite
            if(id1>2 && id2<4){
                if(list.get(id1).get(id2).getText()==list.get(id1-1).get(id2+1).getText()){
                    if(list.get(id1-1).get(id2+1).getText()==list.get(id1-2).get(id2+2).getText()){
                        if(list.get(id1-2).get(id2+2).getText()==list.get(id1-3).get(id2+3).getText()){
                            this.accueil.popUpMessageGagne().showAndWait();
                            this.termineJeu(list);
                        }
                    }  
                }
            }
            // regarde en haut a gauche
            if(id1>2 && id2>2){
                if(list.get(id1).get(id2).getText()==list.get(id1-1).get(id2-1).getText()){
                    if(list.get(id1-1).get(id2-1).getText()==list.get(id1-2).get(id2-2).getText()){
                        if(list.get(id1-2).get(id2-2).getText()==list.get(id1-3).get(id2-3).getText()){
                            this.accueil.popUpMessageGagne().showAndWait();
                            this.termineJeu(list);
                        }
                    }  
                }

            }
            // regarde si il est entre deux couleur a droite et une  a gauche
            if(id2<5 && id2>0){
                if(list.get(id1).get(id2).getText()==list.get(id1).get(id2+1).getText()){
                    if(list.get(id1).get(id2+1).getText()==list.get(id1).get(id2+2).getText()){
                        if(list.get(id1).get(id2).getText()==list.get(id1).get(id2-1).getText()){
                            this.accueil.popUpMessageGagne().showAndWait();
                            this.termineJeu(list);
                        }    
                    }  
                }
            }
            // regarde si il est entre deux couleur a gauche et une  a droite
            if(id2>1 && id2<6){
                if(list.get(id1).get(id2).getText()==list.get(id1).get(id2-1).getText()){
                    if(list.get(id1).get(id2-1).getText()==list.get(id1).get(id2-2).getText()){
                        if(list.get(id1).get(id2).getText()==list.get(id1).get(id2+1).getText()){
                            this.accueil.popUpMessageGagne().showAndWait();
                            this.termineJeu(list);
                        }    
                    }  
                }
            }
        }

    }

    public void termineJeu(List<List<Button>> list){
        for(List<Button> l : list){
            for(Button s : l){
                s.setDisable(true);
            }
        }
    }
}

