import javafx.event.ActionEvent;
import javafx.event.EventHandler;
// import javafx.scene.control.ButtonType;
// import java.util.Optional;

/**
 * Contrôleur à activer lorsque l'on clique sur le bouton Accueil
 */
public class RetourAccueil implements EventHandler<ActionEvent> {
    /**
     * modèle du jeu
     */
    // private MotMystere modelePendu;
    /**
     * vue du jeu
     **/
    private Pendu vuePendu;
    private Accueil accueil;

    /**
     * @param modelePendu modèle du jeu
     * @param vuePendu vue du jeu
     */
    public RetourAccueil(MotMystere modelePendu, Pendu vuePendu,Accueil a) {
        // A implémenter
        // this.modelePendu = modelePendu;
        this.vuePendu = vuePendu;
        this.accueil = a;
    }


    /**
     * L'action consiste à retourner sur la page d'accueil. Il faut vérifier qu'il n'y avait pas une partie en cours
     * @param actionEvent l'événement action
     */
    @Override
    public void handle(ActionEvent actionEvent) {
        // A implémenter
        this.accueil.modeAccueilP(this.accueil.getStage());
        this.vuePendu.getBoutonMaison().setDisable(true);
        this.vuePendu.getBJouer().requestFocus();
    }
}