
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
// import javafx.scene.paint.Color;
import javafx.scene.text.Text;
// import javafx.scene.text.TextAlignment;
// import javafx.scene.control.ButtonBar.ButtonData ;

import java.util.List;

// import java.util.Arrays;
import java.io.File;
import java.util.ArrayList;


/**
 * Vue du jeu du pendu
 */
public class Pendu{
    /**
     * modèle du jeu
     **/
    private Accueil accueil;


    private MotMystere modelePendu;
    public MotMystere getModelePendu() {
        return modelePendu;
    }

    // affecte le niveau souhaiter a la partie
    public void setNiveaubis(int niveau) {
        this.modelePendu = new MotMystere("test.txt", 3, 50, niveau, 10);
    }

    /**
     * Liste qui contient les images du jeu
     */
    private ArrayList<Image> lesImages;
    /**
     * Liste qui contient les noms des niveaux
     */    
    public List<String> niveaux;

    // les différents contrôles qui seront mis à jour ou consultés pour l'affichage
    /**
     * le dessin du pendu
     */
    private ImageView dessin;
    /**
     * le mot à trouver avec les lettres déjà trouvé
     */
    private Text motCrypte;
    /**
     * la barre de progression qui indique le nombre de tentatives
     */


    // permet de mettre le focus sur ce bouton quand tu gagne ou perd 
    private Button nouveauMot;

    private Button boutonClavier;
    public Button getBoutonClavier(){
        return boutonClavier;
    }
    // Le cpt permet d'eviter que quand on lance une partie via l'accueil celui ci ne lance pas de popup
    /**
     * le bouton Paramètre / Engrenage
     */
    private Button boutonParametres;
    public Button getBoutonParametres(){
        return boutonParametres;
    }
    /**
     * le bouton Accueil / Maison
     */    
    private Button boutonMaison;
    public Button getBoutonMaison(){
        return boutonMaison;
    }


    private Button boutonInfo;
    /**
     * le bouton qui permet de (lancer ou relancer une partie
     */ 
    private Button bJouer;
    public Button getBJouer(){
        return bJouer;
    }

    /**
     * initialise les attributs (créer le modèle, charge les images, crée le chrono ...)
     */

    public Pendu(Accueil a) {
        this.modelePendu = new MotMystere("test.txt", 3, 50, MotMystere.FACILE, 10);
        this.lesImages = new ArrayList<Image>();
        this.chargerImages("./img");
        // A terminer d'implementer
        this.bJouer = new Button("Lancer une partie");
        this.bJouer.setOnAction(new ControleurLancerPartie(modelePendu, this, a));
        this.motCrypte = new Text();
        ImageView image3 = new ImageView(new Image("info.png"));
        ImageView image2 = new ImageView(new Image("parametres.png"));
        ImageView image1 = new ImageView(new Image("home.png"));
        image3.setFitWidth(40);
        image2.setFitWidth(40);
        image1.setFitWidth(40);
        image3.setFitHeight(40);
        image2.setFitHeight(40);
        image1.setFitHeight(40);
        this.boutonMaison=new Button("",image1);
        this.boutonParametres=new Button("",image2);
        this.boutonInfo=new Button("",image3);
        this.accueil = a;
    }

    /**
     * @return  le graphe de scène de la vue à partir de methodes précédantes
     */
 
    /**
     * @return le panel contenant le titre du jeu
     */
    public Pane titreP(){
        // A implementer          
        BorderPane top = new BorderPane();
        top.setStyle("-fx-background-color: #D8E8E8;");
        Text toptext = new Text();
        toptext.setText("Jeu du Pendu");
        toptext.setFont(Font.font("Arial", 40));
        top.setLeft(toptext);
        top.setPadding(new Insets(20));
        HBox bouton = new HBox();
        bouton.setSpacing(10);
        bouton.setPadding(new Insets(10));
        ImageView image = new ImageView(new Image("retour.png"));
        image.setFitHeight(40);
        image.setFitWidth(40);
        Button boutonretour = new Button("",image);
        boutonretour.setOnAction(new ControleurRetourChoix(this.accueil));
        this.boutonInfo.setOnAction(new ControleurInfos(this));
        this.boutonMaison.setDisable(true);
        this.boutonMaison.setOnAction(new RetourAccueil(modelePendu, this,this.accueil));
        bouton.getChildren().addAll(boutonretour,this.boutonMaison,this.boutonParametres,this.boutonInfo);
        top.setRight(bouton);
        return top;
    }


    public Pane fenetreJeu(){
        dessin = new ImageView(lesImages.get(modelePendu.getNbErreursMax()-modelePendu.getNbErreursRestants()));
        String ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        BorderPane jeu = new BorderPane();
        jeu.setPadding(new Insets(20));
        BorderPane top = new BorderPane();
        BorderPane right = new BorderPane();
        motCrypte.setText(this.modelePendu.getMotCrypte());
        motCrypte.setFont(Font.font("Arial",FontWeight.BOLD, 28));
        TilePane lesLettres = new TilePane();
        // Le clavier
        for(int i = 0; i<26;i++){
            char lettre = ALPHABET.charAt(i);
            this.boutonClavier = new Button(String.valueOf(lettre));
            this.boutonClavier.setOnAction( new ControleurLettres(modelePendu, this));
            this.boutonClavier.setOnKeyPressed( new ControleurLettresClavier(modelePendu, this));
            lesLettres.getChildren().add(this.boutonClavier);
        }
        
        this.nouveauMot = new Button("Nouveau Mot");
        this.nouveauMot.setOnAction(new ControleurLancerPartie(modelePendu, this,this.accueil));
        right.setTop(this.nouveauMot);
        top.setCenter(motCrypte);
        jeu.setBottom(lesLettres);
        jeu.setCenter(dessin);
        jeu.setTop(top);
        jeu.setRight(right);
        return jeu;
    }

    /**
     * @return la fenêtre d'accueil sur laquelle on peut choisir les paramètres de jeu
     */
    public Pane fenetreAccueil(){
        VBox accueil = new VBox();
        accueil.setSpacing(10);
        accueil.setPadding(new Insets(20));
        accueil.setAlignment(Pos.TOP_LEFT);
        TitledPane title = new TitledPane();
        title.setText("Niveaux de difficultés");
        ToggleGroup group = new ToggleGroup();
        RadioButton bouton1 = new RadioButton("Facile");
        RadioButton bouton2 = new RadioButton("Moyen");
        RadioButton bouton3 = new RadioButton("Difficile");
        RadioButton bouton4 = new RadioButton("Expert");
        bouton1.setOnAction(new ControleurNiveau(modelePendu, this));
        bouton2.setOnAction(new ControleurNiveau(modelePendu, this));
        bouton3.setOnAction(new ControleurNiveau(modelePendu, this));
        bouton4.setOnAction(new ControleurNiveau(modelePendu, this));

        bouton1.setToggleGroup(group);
        bouton1.setSelected(true);
        bouton2.setToggleGroup(group);
        bouton3.setToggleGroup(group);
        bouton4.setToggleGroup(group);
        accueil.getChildren().add(this.bJouer);
        VBox radio = new VBox();
        radio.setSpacing(5);
        radio.setPadding(new Insets(20));
        radio.getChildren().addAll(bouton1,bouton2,bouton3,bouton4);
        title.setContent(radio);
        title.setCollapsible(false);
        accueil.getChildren().add(title);
        return accueil;
    }

    /**
     * charge les images à afficher en fonction des erreurs
     * @param repertoire répertoire où se trouvent les images
     */
    private void chargerImages(String repertoire){
        for (int i=0; i<this.modelePendu.getNbErreursMax()+1; i++){
            File file = new File(repertoire+"/pendu"+i+".png");
            System.out.println(file.toURI().toString());
            this.lesImages.add(new Image(file.toURI().toString()));
        }
    }

    // public void modeAccueil(){
    //     // A implementer
    //     this.panelCentral.setCenter(fenetreAccueil());
    //     cpt = 0;// Voir la variable au debut

    // }
    
    // public void modeJeu(){
    //     // A implementer
    //     this.panelCentral.setCenter(fenetreJeu());
    //     cpt = 1;// Voir la variable au debut
    // }
    
    public void modeParametres(){
        // A implémenter
    }

    /** lance une partie */
    

    /**
     * raffraichit l'affichage selon les données du modèle
     */
    public void majAffichage(){
        // A implementer
        dessin.setImage(lesImages.get(modelePendu.getNbErreursMax()-modelePendu.getNbErreursRestants()));
        motCrypte.setText(modelePendu.getMotCrypte());
        System.out.println("Le mot a trouver : "+this.modelePendu.getMotCrypte());
        System.out.println("Le mot a trouver : "+this.modelePendu.getMotATrouve());
        if(modelePendu.gagne()){
            popUpMessageGagne().showAndWait();
            this.nouveauMot.requestFocus();
        }
        else if(modelePendu.perdu()){
            popUpMessagePerdu().showAndWait();
            this.nouveauMot.requestFocus();
        }
    }



    public Alert popUpPartieEnCours(){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION,"La partie est en cours!\n Etes-vous sûr de l'interrompre ?", ButtonType.YES, ButtonType.NO);
        alert.setTitle("Attention");
        return alert;
    }
        
    public Alert popUpReglesDuJeu(){
        // A implementer
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        return alert;
    }
    
    public Alert popUpMessageGagne(){
        // A implementer
        Alert alert = new Alert(Alert.AlertType.INFORMATION,"Bravo ! Vous avez gagné !",ButtonType.OK);      
        alert.setTitle("Jeu du Pendu");
        alert.setHeaderText("Vous avez gagné :)");
        return alert;
    }
    
    public Alert popUpMessagePerdu(){
        // A implementer    
        Alert alert = new Alert(Alert.AlertType.INFORMATION,"Vous avez perdu\n Le mot à trouver était "+this.modelePendu.getMotATrouve());
        alert.setTitle("Jeu du Pendu");
        alert.setHeaderText("Vous avez perdu (0)_(0)");
        return alert;
    }



}