import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;

public class ControleurRecommencer implements EventHandler<ActionEvent>{

    private Accueil accueil;

    public ControleurRecommencer(Accueil a){

        this.accueil = a;
    }
    
    @Override
    public void handle(ActionEvent event) {
        this.accueil.lancePartiePuissance();
        
    }
    
}