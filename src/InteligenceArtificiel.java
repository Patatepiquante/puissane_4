import javafx.scene.control.Button;
import javafx.scene.image.ImageView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.swing.plaf.basic.BasicComboBoxUI.ItemHandler;

public class InteligenceArtificiel {
    private Accueil accueil;
    private Puissance4test puissance;

    public InteligenceArtificiel(Accueil a, Puissance4test p){
        this.accueil = a;
        this.puissance = p;

    }

    public Button contre(int id1, int id2, List<List<Button>> list){
        System.out.println("1");
        // regarde a droite 
        // 0 2 1 1 0
        if(id2<4){
            if(list.get(id1).get(id2).getText()==list.get(id1).get(id2+1).getText()){
                if(list.get(id1).get(id2).getText()==list.get(id1).get(id2+2).getText()){
                    if(list.get(id1).get(id2+3).isDisable()==false){
                        list.get(id1-1).get(id2+3).setDisable(false);
                        return list.get(id1).get(id2+3);
                    }
                        
                }  
            }
        }
        if(id2<5 && id2>0){
            if(list.get(id1).get(id2).getText()==list.get(id1).get(id2+1).getText()){
                if(list.get(id1).get(id2).getText()==list.get(id1).get(id2+2).getText()){
                    if(list.get(id1).get(id2-1).isDisable()==false){
                        list.get(id1-1).get(id2-1).setDisable(false);
                        return list.get(id1).get(id2-1);
                    }
                        
                }  
            }
        }
        
        // regarde a gauche
        // 0 1 1 2 0
        if(id2>2){
            if(list.get(id1).get(id2).getText()==list.get(id1).get(id2-1).getText()){
                if(list.get(id1).get(id2).getText()==list.get(id1).get(id2-2).getText()){
                    if(list.get(id1).get(id2-3).isDisable()==false ){
                        list.get(id1-1).get(id2-3).setDisable(false);
                        return list.get(id1).get(id2-3);
                        
                    }
                }  
            }
        }
        if(id2>1 && id2<6){
            if(list.get(id1).get(id2).getText()==list.get(id1).get(id2-1).getText()){
                if(list.get(id1).get(id2).getText()==list.get(id1).get(id2-2).getText()){
                    if(list.get(id1).get(id2+1).isDisable()==false){
                        list.get(id1-1).get(id2+1).setDisable(false);
                        return list.get(id1).get(id2+1);
                    }
                }  
            }
        }

        // 1 0 2 1
        if(id2>1 && id2<6){
            if(list.get(id1).get(id2).getText()==list.get(id1).get(id2+1).getText()){
                if(list.get(id1).get(id2).getText()==list.get(id1).get(id2-2).getText()){
                    if(list.get(id1).get(id2-1).isDisable()==false ){
                        list.get(id1-1).get(id2-1).setDisable(false);
                        return list.get(id1).get(id2-1);
                        
                    }
                }  
            }
        }

        // 1 0 1 2
        if(id2>2){
            if(list.get(id1).get(id2).getText()==list.get(id1).get(id2-1).getText()){
                if(list.get(id1).get(id2).getText()==list.get(id1).get(id2-3).getText()){
                    if(list.get(id1).get(id2-2).isDisable()==false ){
                        list.get(id1-1).get(id2-2).setDisable(false);
                        return list.get(id1).get(id2-2);
                        
                    }
                }  
            }
        }
        // 0 1 2 1 0
        if(id2>0 && id2<5){
            if(list.get(id1).get(id2).getText()==list.get(id1).get(id2-1).getText()){
                if(list.get(id1).get(id2).getText()==list.get(id1).get(id2+1).getText()){
                    if(list.get(id1).get(id2+2).isDisable()==false ){
                        list.get(id1-1).get(id2+2).setDisable(false);
                        return list.get(id1).get(id2+2);
                        
                    }
                }  
            }
        }
        if(id2>1 && id2<6){
            if(list.get(id1).get(id2).getText()==list.get(id1).get(id2-1).getText()){
                if(list.get(id1).get(id2).getText()==list.get(id1).get(id2+1).getText()){
                    if(list.get(id1).get(id2-2).isDisable()==false ){
                        list.get(id1-1).get(id2-2).setDisable(false);
                        return list.get(id1).get(id2-2);
                        
                    }
                }  
            }
        }
        // 2 0 1 1
        if(id2<4){
            if(list.get(id1).get(id2).getText()==list.get(id1).get(id2+2).getText()){
                if(list.get(id1).get(id2).getText()==list.get(id1).get(id2+3).getText()){
                    if(list.get(id1).get(id2+1).isDisable()==false ){   
                        list.get(id1-1).get(id2+1).setDisable(false);
                        return list.get(id1).get(id2+1);
                        
                    }
                }  
            }
        }

        // 1 1 0 2
        if(id2>2){
            if(list.get(id1).get(id2).getText()==list.get(id1).get(id2-2).getText()){
                if(list.get(id1).get(id2).getText()==list.get(id1).get(id2-3).getText()){
                    if(list.get(id1).get(id2-1).isDisable()==false ){
                        list.get(id1-1).get(id2-1).setDisable(false);
                        return list.get(id1).get(id2-1);
                        
                    }
                }  
            }
        }

        // 1 2 0 1
        if(id2>0 && id2<5){
            if(list.get(id1).get(id2).getText()==list.get(id1).get(id2+2).getText()){
                if(list.get(id1).get(id2).getText()==list.get(id1).get(id2-1).getText()){
                    if(list.get(id1).get(id2+1).isDisable()==false ){
                        list.get(id1-1).get(id2+1).setDisable(false);
                        return list.get(id1).get(id2+1);
                        
                    }
                }  
            }
        }
        // 2 1 0 1 
        if(id2<4){
            if(list.get(id1).get(id2).getText()==list.get(id1).get(id2+1).getText()){
                if(list.get(id1).get(id2).getText()==list.get(id1).get(id2+3).getText()){
                    if(list.get(id1).get(id2+2).isDisable()==false ){
                        list.get(id1-1).get(id2+2).setDisable(false);
                        return list.get(id1).get(id2+2);
                        
                    }
                }  
            }
        }

        
        // // regarde en haut
        // 0
        // 2
        // 1
        // 1
        if (id1<4){
            if(list.get(id1).get(id2).getText()==list.get(id1+1).get(id2).getText()){
                if(list.get(id1).get(id2).getText()==list.get(id1+2).get(id2).getText()){
                    if(list.get(id1-1).get(id2).isDisable()==false){
                        list.get(id1-2).get(id2).setDisable(false);
                        return list.get(id1-1).get(id2);
                        
                    }
                }  
            }
        }
        
        // //regarde en bas a droite et en haut a gauche  
        // 1
        //  0
        //   1
        //    2
        if(id1>2 && id2>2){
            if(list.get(id1).get(id2).getText()==list.get(id1-1).get(id2-1).getText()){
                if(list.get(id1).get(id2).getText()==list.get(id1-3).get(id2-3).getText()){
                    if(list.get(id1-2).get(id2-2).isDisable()==false){
                        list.get(id1-3).get(id2-2).setDisable(false);
                        return list.get(id1-2).get(id2-2);
                        
                    }
                }  
            }
        }

        // 1
        //  0
        //   2
        //    1
        if(id1>1 && id1<5 && id2>1 && id2<6){
            if(list.get(id1).get(id2).getText()==list.get(id1+1).get(id2+1).getText()){
                if(list.get(id1).get(id2).getText()==list.get(id1-2).get(id2-2).getText()){
                    if(list.get(id1-1).get(id2-1).isDisable()==false){
                        list.get(id1-2).get(id2-1).setDisable(false);
                        return list.get(id1-1).get(id2-1);
                        
                    }
                }  
            }
        }
        // 0
        //  1
        //   2
        //    1
        //     0
        if(id1>1 && id1<5 && id2>1 && id2<6){
            if(list.get(id1).get(id2).getText()==list.get(id1+1).get(id2+1).getText()){
                if(list.get(id1).get(id2).getText()==list.get(id1-1).get(id2-1).getText()){
                    if(list.get(id1-2).get(id2-2).isDisable()==false){
                        list.get(id1-3).get(id2-2).setDisable(false);
                        return list.get(id1-2).get(id2-2);
                        
                    }
                }  
            }
        }
        if(id1>0 && id1<4 && id2>0 && id2<5){
            if(list.get(id1).get(id2).getText()==list.get(id1+1).get(id2+1).getText()){
                if(list.get(id1).get(id2).getText()==list.get(id1-1).get(id2-1).getText()){
                    if(list.get(id1+2).get(id2+2).isDisable()==false){
                        list.get(id1+1).get(id2+2).setDisable(false);
                        return list.get(id1+2).get(id2+2);
                        
                    }
                }  
            }
        }
        // 0
        //  2
        //   1
        //    1
        //     0
        if(id1<3 && id2<4){
            if(list.get(id1).get(id2).getText()==list.get(id1+1).get(id2+1).getText()){
                if(list.get(id1).get(id2).getText()==list.get(id1+2).get(id2+2).getText()){
                    if(list.get(id1+3).get(id2+3).isDisable()==false){
                        list.get(id1+2).get(id2+3).setDisable(false);
                        return list.get(id1+3).get(id2+3);
                        
                    }
                }  
            }
        }
        if(id1>0 && id1<5 && id2>0 && id2<6){
            if(list.get(id1).get(id2).getText()==list.get(id1+1).get(id2+1).getText()){
                if(list.get(id1).get(id2).getText()==list.get(id1+2).get(id2+2).getText()){
                    if(list.get(id1-1).get(id2-1).isDisable()==false){
                        list.get(id1-2).get(id2-1).setDisable(false);
                        return list.get(id1-1).get(id2-1);
                        
                    }
                }  
            }
        }
        // 2
        //  0
        //   1
        //    1
        if(id1<3 && id2<4){
            if(list.get(id1).get(id2).getText()==list.get(id1+3).get(id2+3).getText()){
                if(list.get(id1).get(id2).getText()==list.get(id1+2).get(id2+2).getText()){
                    if(list.get(id1+1).get(id2+1).isDisable()==false){
                        list.get(id1).get(id2+1).setDisable(false);
                        return list.get(id1+1).get(id2+1);
                        
                    }
                }  
            }
        }
        // 1
        //  1
        //   0
        //    2
        if(id1>2 && id2>2){
            if(list.get(id1).get(id2).getText()==list.get(id1-3).get(id2-3).getText()){
                if(list.get(id1).get(id2).getText()==list.get(id1-2).get(id2-2).getText()){
                    if(list.get(id1-1).get(id2-1).isDisable()==false){
                        list.get(id1-2).get(id2-1).setDisable(false);
                        return list.get(id1-1).get(id2-1);
                        
                    }
                }  
            }
        }
        // 0
        //  1
        //   1
        //    2
        //     0
        if(id1>2 && id2>2){
            if(list.get(id1).get(id2).getText()==list.get(id1-1).get(id2-1).getText()){
                if(list.get(id1).get(id2).getText()==list.get(id1-2).get(id2-2).getText()){
                    if(list.get(id1-3).get(id2-3).isDisable()==false){
                        list.get(id1-4).get(id2-3).setDisable(false);
                        return list.get(id1-3).get(id2-3);
                        
                    }
                }  
            }
        }
        if(id1>1 && id1<5 && id2>1 && id2<6){
            if(list.get(id1).get(id2).getText()==list.get(id1-1).get(id2-1).getText()){
                if(list.get(id1).get(id2).getText()==list.get(id1-2).get(id2-2).getText()){
                    if(list.get(id1+1).get(id2+1).isDisable()==false){
                        list.get(id1).get(id2+1).setDisable(false);
                        return list.get(id1+1).get(id2+1);
                        
                    }
                }  
            }
        }
        // 1
        //  2
        //   0
        //    1
        if(id1>0 && id1<4 && id2>0 && id2<5){
            if(list.get(id1).get(id2).getText()==list.get(id1-1).get(id2-1).getText()){
                if(list.get(id1).get(id2).getText()==list.get(id1+2).get(id2+2).getText()){
                    if(list.get(id1+1).get(id2+1).isDisable()==false){
                        list.get(id1).get(id2+1).setDisable(false);
                        return list.get(id1+1).get(id2+1);
                        
                    }
                }  
            }
        }
        // 2
        //  1
        //   0
        //    1
        if(id1<3 && id2<4){
            if(list.get(id1).get(id2).getText()==list.get(id1+1).get(id2+1).getText()){
                if(list.get(id1).get(id2).getText()==list.get(id1+3).get(id2+3).getText()){
                    if(list.get(id1+2).get(id2+2).isDisable()==false){
                        list.get(id1+1).get(id2+2).setDisable(false);
                        return list.get(id1+2).get(id2+2);
                        
                    }
                }  
            }
        }
        //regarde en bas a gauche
        //     1
        //    0
        //   1
        //  2
        if(id1>2 && id2<4){
            if(list.get(id1).get(id2).getText()==list.get(id1-3).get(id2+3).getText()){
                if(list.get(id1).get(id2).getText()==list.get(id1-1).get(id2+1).getText()){
                    if(list.get(id1-2).get(id2+2).isDisable()==false){
                        list.get(id1-3).get(id2+2).setDisable(false);
                        return list.get(id1-2).get(id2+2);
                        
                    }
                }  
            }
        }
        //     1
        //    0
        //   2
        //  1
        if(id1>1 && id1<5 && id2<5 && id2>0){
            if(list.get(id1).get(id2).getText()==list.get(id1-2).get(id2+2).getText()){
                if(list.get(id1).get(id2).getText()==list.get(id1+1).get(id2-1).getText()){
                    if(list.get(id1-1).get(id2+1).isDisable()==false){
                        list.get(id1-2).get(id2+1).setDisable(false);
                        return list.get(id1-1).get(id2+1);
                        
                    }
                }  
            }
        }
        //      0
        //     1
        //    2
        //   1
        //  0
        if(id1>1 && id1<5 && id2<5 && id2>0){
            if(list.get(id1).get(id2).getText()==list.get(id1-1).get(id2+1).getText()){
                if(list.get(id1).get(id2).getText()==list.get(id1+1).get(id2-1).getText()){
                    if(list.get(id1-2).get(id2+2).isDisable()==false){
                        list.get(id1-3).get(id2+2).setDisable(false);
                        return list.get(id1-2).get(id2+2);
                        
                        
                    }
                }  
            }
        }
        if(id1>0 && id1<4 && id2<6 && id2>1){
            if(list.get(id1).get(id2).getText()==list.get(id1-1).get(id2+1).getText()){
                if(list.get(id1).get(id2).getText()==list.get(id1+1).get(id2-1).getText()){
                    if(list.get(id1+2).get(id2-2).isDisable()==false){
                        list.get(id1+1).get(id2-2).setDisable(false);
                        return list.get(id1+2).get(id2-2);
                        
                        
                    }
                }  
            }
        }
        //      0
        //     2
        //    1
        //   1
        //  0
        if(id1>0 && id1<4 && id2<6 && id2>1){
            if(list.get(id1).get(id2).getText()==list.get(id1+2).get(id2-2).getText()){
                if(list.get(id1).get(id2).getText()==list.get(id1+1).get(id2-1).getText()){
                    if(list.get(id1-1).get(id2+1).isDisable()==false){
                        list.get(id1-2).get(id2+1).setDisable(false);
                        return list.get(id1-1).get(id2+1);
                        
                        
                        
                    }
                }  
            }
        }
        if(id1<3 && id2>2){
            if(list.get(id1).get(id2).getText()==list.get(id1+1).get(id2-1).getText()){
                if(list.get(id1).get(id2).getText()==list.get(id1+2).get(id2-2).getText()){
                    if(list.get(id1+3).get(id2-3).isDisable()==false){
                        list.get(id1+2).get(id2-3).setDisable(false);
                        return list.get(id1+3).get(id2-3);
                    }
                }  
            }
        }
        //      0
        //     1
        //    1
        //   2
        //  0
        if(id1>1 && id1<5 && id2<5 && id2>0){
            if(list.get(id1).get(id2).getText()==list.get(id1-1).get(id2+1).getText()){
                if(list.get(id1).get(id2).getText()==list.get(id1-2).get(id2+2).getText()){
                    if(list.get(id1+1).get(id2-1).isDisable()==false){
                        list.get(id1).get(id2-1).setDisable(false);
                        return list.get(id1+1).get(id2-1);
                        
                    }
                }  
            }
        }
        if(id1>2 && id2<4){
            if(list.get(id1).get(id2).getText()==list.get(id1-1).get(id2+1).getText()){
                if(list.get(id1).get(id2).getText()==list.get(id1-2).get(id2+2).getText()){
                    if(list.get(id1-3).get(id2+3).isDisable()==false){
                        list.get(id1-4).get(id2+3).setDisable(false);
                        return list.get(id1-3).get(id2+3);
                        
                    }
                }  
            }
        }
        //     2
        //    0
        //   1
        //  1
        if(id1<3 && id2>2){
            if(list.get(id1).get(id2).getText()==list.get(id1+2).get(id2-2).getText()){
                if(list.get(id1).get(id2).getText()==list.get(id1+3).get(id2-3).getText()){
                    if(list.get(id1+1).get(id2-1).isDisable()==false){
                        list.get(id1).get(id2-1).setDisable(false);
                        return list.get(id1+1).get(id2-1);
                        
                    }
                }  
            }
        }
        //     1
        //    1
        //   0
        //  2
        if(id1>2&& id2<4){
            if(list.get(id1).get(id2).getText()==list.get(id1-2).get(id2+2).getText()){
                if(list.get(id1).get(id2).getText()==list.get(id1-3).get(id2+3).getText()){
                    if(list.get(id1-1).get(id2+1).isDisable()==false){
                        list.get(id1-2).get(id2+1).setDisable(false);
                        return list.get(id1-1).get(id2+1);
                        
                    }
                }  
            }
        }
        //     1
        //    2
        //   0
        //  1
        if(id1>0 && id1<4 && id2<6 && id2>1){
            if(list.get(id1).get(id2).getText()==list.get(id1-1).get(id2+1).getText()){
                if(list.get(id1).get(id2).getText()==list.get(id1+2).get(id2-2).getText()){
                    if(list.get(id1+1).get(id2-1).isDisable()==false){
                        list.get(id1).get(id2-1).setDisable(false);
                        return list.get(id1+1).get(id2-1);
                        
                    }
                }  
            }
        }
        //     2
        //    1
        //   0
        //  1
        if(id1<3 && id2>2){
            if(list.get(id1).get(id2).getText()==list.get(id1+3).get(id2-3).getText()){
                if(list.get(id1).get(id2).getText()==list.get(id1+1).get(id2-1).getText()){
                    if(list.get(id1+2).get(id2-2).isDisable()==false){
                        list.get(id1+1).get(id2-2).setDisable(false);
                        return list.get(id1+2).get(id2-2);
                        
                    }
                }  
            }
        }
        return null;
    }

    public Button cherchevictoire(int id1, int id2, List<List<Button>> list){
        System.out.println("hello");
        if(list.get(id1).get(id2+1).isDisable()==false){ // Droite
            list.get(id1-1).get(id2+1).setDisable(false);
            return list.get(id1).get(id2+1);
        }
        if(list.get(id1).get(id2+1).isDisable()==true && list.get(id1).get(id2+1).getText()=="J"){
            if(list.get(id1).get(id2+2).isDisable()==false){
                list.get(id1-1).get(id2+2).setDisable(false);
                return list.get(id1).get(id2+2);
            }
        }
        if(list.get(id1).get(id2+1).isDisable()==true && list.get(id1).get(id2+1).getText()=="J"){
            if(list.get(id1).get(id2+2).isDisable()==true && list.get(id1).get(id2+2).getText()=="J"){
                if(list.get(id1).get(id2+3).isDisable()==false){
                    return list.get(id1).get(id2+3);
                }
            }
        }
        if(list.get(id1).get(id2-1).isDisable()==false){ // gauche
            list.get(id1-1).get(id2-1).setDisable(false);
            return list.get(id1).get(id2-1);
        }
        if(list.get(id1).get(id2-1).isDisable()==true && list.get(id1).get(id2-1).getText()=="J"){
            if(list.get(id1).get(id2-2).isDisable()==false){
                list.get(id1-1).get(id2-2).setDisable(false);
                return list.get(id1).get(id2-2);
            }
        }
        if(list.get(id1).get(id2-1).isDisable()==true && list.get(id1).get(id2-1).getText()=="J"){
            if(list.get(id1).get(id2-2).isDisable()==true && list.get(id1).get(id2-2).getText()=="J"){
                if(list.get(id1).get(id2-3).isDisable()==false){
                    return list.get(id1).get(id2-3);
                }
            }
        }
        return null;    
    }

    public Button alea(List<List<Button>> list){
        System.out.println("2");
        List<Button> lb = new ArrayList<>();
        List<Button> lbsup = new ArrayList<>();
        int ind1 = 0;
        int ind2 = 0;
        for(List<Button> l: list){
            for(Button b : l){
                if(b.isDisable()==false){
                    lb.add(b);
                    lbsup.add(list.get(ind1-1).get(ind2));
                }
                ind2+=1;
            }
            ind2=0;
            ind1+=1;
        } 
        int rand = (int)(Math.random()*7)+1;
        lbsup.get(rand).setDisable(false);
        return lb.get(rand);
    }
}
