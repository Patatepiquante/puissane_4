import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;

public class ControleurRecommencerIa implements EventHandler<ActionEvent>{

    private Accueil accueil;

    public ControleurRecommencerIa(Accueil a){

        this.accueil = a;
    }
    
    @Override
    public void handle(ActionEvent event) {
        this.accueil.lancePartiePuissanceIa();
        
    }
    
}