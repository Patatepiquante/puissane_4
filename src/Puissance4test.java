import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.beans.value.WritableValue;
import javafx.geometry.Insets;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;

import java.util.List;
import java.util.concurrent.BrokenBarrierException;
import java.util.zip.CRC32;

import javax.smartcardio.CardPermission;

import java.io.File;
import java.sql.Time;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;



public class Puissance4test {
    private Accueil accueil;
    private List<List<Button>> listboutons;
    private JeuPuissance4 modelPuissance;
    private Text etatjoueur;
    private Button prochainjoueur;
    private int iajouer;

    public int getIajouer() {
        return iajouer;
    }

    public void setIajouer(int iajouer) {
        this.iajouer = iajouer;
    }

    public Button getProchainjoueur() {
        return prochainjoueur;
    }

    public Puissance4test(Accueil a){
        this.accueil = a;   
        this.listboutons = new ArrayList<>(); 
        this.modelPuissance = new JeuPuissance4(a);
        this.etatjoueur = new Text("Joueur:");
        this.etatjoueur.setFont(Font.font("Arial",FontWeight.BOLD, 14)); 
        this.prochainjoueur = new Button();
        this.iajouer = 1;
        

    }

    public Pane titrejeuPuis(){
        // A implementer          
        BorderPane top = new BorderPane();
        top.setStyle("-fx-background-color: #D8E8E8;");
        Text toptext = new Text();
        toptext.setText("Le Puissance 4");
        toptext.setFont(Font.font("Arial", 40));
        top.setLeft(toptext);
        top.setPadding(new Insets(20));
        HBox bouton = new HBox();
        bouton.setSpacing(10);
        bouton.setPadding(new Insets(10));
        ImageView image1 = new ImageView(new Image("recommencer.png"));
        ImageView image = new ImageView(new Image("retour.png"));
        ImageView image2 = new ImageView(new Image("home.png"));
        image2.setFitHeight(40);
        image2.setFitWidth(40);
        image.setFitHeight(40);
        image.setFitWidth(40);
        image1.setFitHeight(40);
        image1.setFitWidth(40);
        Button boutonratouracc = new Button("",image2);
        Button boutonretour = new Button("",image);
        Button boutonrecommencer = new Button("",image1);
        boutonratouracc.setOnAction(new ControleurAccueilPuis(this.accueil));
        boutonretour.setOnAction(new ControleurRetourChoix(this.accueil));
        boutonrecommencer.setOnAction(new ControleurRecommencer(this.accueil));
        bouton.getChildren().addAll(boutonretour,boutonratouracc,boutonrecommencer);
        top.setRight(bouton);
        return top;
    }

    public Pane titrejeuPuisIa(){
        // A implementer          
        BorderPane top = new BorderPane();
        top.setStyle("-fx-background-color: #D8E8E8;");
        Text toptext = new Text();
        toptext.setText("Le Puissance 4");
        toptext.setFont(Font.font("Arial", 40));
        top.setLeft(toptext);
        top.setPadding(new Insets(20));
        HBox bouton = new HBox();
        bouton.setSpacing(10);
        bouton.setPadding(new Insets(10));
        ImageView image1 = new ImageView(new Image("recommencer.png"));
        ImageView image = new ImageView(new Image("retour.png"));
        ImageView image2 = new ImageView(new Image("home.png"));
        image2.setFitHeight(40);
        image2.setFitWidth(40);
        image.setFitHeight(40);
        image.setFitWidth(40);
        image1.setFitHeight(40);
        image1.setFitWidth(40);
        Button boutonratouracc = new Button("",image2);
        Button boutonretour = new Button("",image);
        Button boutonrecommencer = new Button("",image1);
        boutonratouracc.setOnAction(new ControleurAccueilPuis(this.accueil));
        boutonretour.setOnAction(new ControleurRetourChoix(this.accueil));
        boutonrecommencer.setOnAction(new ControleurRecommencerIa(this.accueil));
        bouton.getChildren().addAll(boutonretour,boutonratouracc,boutonrecommencer);
        top.setRight(bouton);
        return top;
    }

    public Pane titreaccPuis(){
        // A implementer          
        BorderPane top = new BorderPane();
        top.setStyle("-fx-background-color: #D8E8E8;");
        Text toptext = new Text();
        toptext.setText("Le Puissance 4");
        toptext.setFont(Font.font("Arial", 40));
        top.setLeft(toptext);
        top.setPadding(new Insets(20));
        HBox bouton = new HBox();
        bouton.setSpacing(10);
        bouton.setPadding(new Insets(10));
        ImageView image = new ImageView(new Image("retour.png"));
        image.setFitHeight(40);
        image.setFitWidth(40);
        Button boutonretour = new Button("",image);
        boutonretour.setOnAction(new ControleurRetourChoix(this.accueil));
        bouton.getChildren().addAll(boutonretour);
        top.setRight(bouton);
        return top;
    }

    public Pane fenetrePuis(){
        this.listboutons = new ArrayList<>();
        BorderPane jeupuis = new BorderPane();
        String jetonr = "jetonR.png";
        String jetonj = "jetonJ.png";
        ImageView etat = new ImageView(new Image("jetonR.png"));
        etat.setFitHeight(60);
        etat.setFitWidth(60);
        this.prochainjoueur = new Button("",etat);
        this.prochainjoueur.setDisable(true);
        this.prochainjoueur.setAlignment(Pos.CENTER);
        ImageView imagevide = new ImageView(new Image("vide.png"));
        imagevide.setFitHeight(60);
        imagevide.setFitWidth(60);
        jeupuis.setPadding(new Insets(20));
        VBox right = new VBox();
        right.setPadding(new Insets(20));
        right.setSpacing(20);
        TilePane cases = new TilePane();
        cases.setMaxWidth(620);
        for(int e = 0;e<42;e++){
            List<Button> copie = new ArrayList<>();
            for(int i = 0;i<7;i++){
                Button boutonCases = new Button("",imagevide);
                if(e<35){
                    boutonCases.setDisable(true);
                }
                boutonCases.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                boutonCases.setOnAction(new ControleurBoutonJeton(this,this.modelPuissance, this.accueil,jetonj,jetonr));
                cases.getChildren().add(boutonCases);
                copie.add(boutonCases);
                e+=1;
                if (i == 6){
                    e-=1;
                }
            }
            this.listboutons.add(copie);
        }
        right.getChildren().addAll(this.etatjoueur,this.prochainjoueur);
        cases.setPadding(new Insets(20));
        jeupuis.setCenter(cases);
        jeupuis.setRight(right);
        return jeupuis;
    }

    public Pane fenetreaccueilPuis(){
        BorderPane accpuis = new BorderPane();
        HBox center = new HBox();
        ImageView image = new ImageView(new Image("joueur.png"));
        ImageView image1 = new ImageView(new Image("robot.png"));
        image.setFitHeight(80);
        image.setFitWidth(80);
        image1.setFitHeight(80);
        image1.setFitWidth(80);
        Button robot = new Button("",image1);
        Button joueurs = new Button("",image);
        center.setAlignment(Pos.CENTER);
        center.setSpacing(40);
        robot.setOnAction(new ControleurLancerIaPuis(this.accueil));
        joueurs.setOnAction(new ControleurLancerJoueurPuis(this.accueil));
        center.getChildren().addAll(joueurs,robot);
        accpuis.setCenter(center);
        return accpuis;
    }

    public Pane fenetrePuisIA(){
        System.out.println("cc");
        this.listboutons = new ArrayList<>();
        BorderPane jeupuis = new BorderPane();
        String jetonr = "jetonR.png";
        String jetonj = "jetonJ.png";
        ImageView etat = new ImageView(new Image("jetonR.png"));
        etat.setFitHeight(60);
        etat.setFitWidth(60);
        this.prochainjoueur = new Button("",etat);
        this.prochainjoueur.setDisable(true);
        this.prochainjoueur.setAlignment(Pos.CENTER);
        ImageView imagevide = new ImageView(new Image("vide.png"));
        imagevide.setFitHeight(60);
        imagevide.setFitWidth(60);
        jeupuis.setPadding(new Insets(20));
        VBox right = new VBox();
        right.setPadding(new Insets(20));
        right.setSpacing(20);
        TilePane cases = new TilePane();
        cases.setMaxWidth(620);
        for(int e = 0;e<42;e++){
            List<Button> copie = new ArrayList<>();
            for(int i = 0;i<7;i++){
                Button boutonCases = new Button("",imagevide);
                if(e<35){
                    boutonCases.setDisable(true);
                }
                boutonCases.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                boutonCases.setOnAction(new ControleurBoutonJetonIa(this,this.modelPuissance, this.accueil,jetonj,jetonr));
                cases.getChildren().add(boutonCases);
                copie.add(boutonCases);
                e+=1;
                if (i == 6){
                    e-=1;
                }
            }
            this.listboutons.add(copie);
        }
        right.getChildren().addAll(this.etatjoueur,this.prochainjoueur);
        cases.setPadding(new Insets(20));
        jeupuis.setCenter(cases);
        jeupuis.setRight(right);
        return jeupuis;
    }

    public List<List<Button>> getListboutons() {
        return this.listboutons;
    }



    
}
