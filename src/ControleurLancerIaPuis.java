import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;

public class ControleurLancerIaPuis implements EventHandler<ActionEvent>{

    private Accueil accueil;

    public ControleurLancerIaPuis(Accueil a){

        this.accueil = a;
    }
    
    @Override
    public void handle(ActionEvent event) {
        this.accueil.modeiaPuis(this.accueil.getStage());
        
    }
    
}